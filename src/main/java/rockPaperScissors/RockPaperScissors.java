package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            System.out.print("Let's play round "+roundCounter + "\n");
            String myMove = readInput("Your choice (Rock/Paper/Scissors)?");
        //I need to verify that the choice the user took is valid:
        if (!myMove.equals("rock") && !myMove.equals("paper") && !myMove.equals("scissors")) {
            System.out.println("I do not understand "+myMove+". Could you try again?");
        } else{
            //Now I have to generate a random move from the computer:
        int rand = (int)(Math.random() * 3);
        String opponentMove = "";
        if (rand==0){
            opponentMove = "rock";
        } else if(rand==1){
            opponentMove = "paper";
        } else {
            opponentMove = "scissors";
        }
        //I have to check who won:
        if (myMove.equals(opponentMove)) {
            System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". It's a tie!");
        } else if((myMove.equals("rock") && opponentMove.equals("scissors")) || (myMove.equals("scissors") && opponentMove.equals("paper")) || (myMove.equals("paper") && opponentMove.equals("rock"))) {
            System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". Human wins!");
            humanScore = humanScore + 1;
        } else {
            System.out.println("Human chose " + myMove + ", computer chose " + opponentMove + ". computer wins!");
            computerScore = computerScore + 1;
        }
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        String playAgain = readInput("Do you wish to continue playing? (y/n)?");
        if (playAgain.equals("n")){
            System.out.println("Bye bye :)");
            break;
        }
        }
        roundCounter+=1;
    }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
